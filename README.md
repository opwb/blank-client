#### ��������� ������� ��� ������

##### ����� �������
������� ����� ������� �� �������� � \Client\Api\Requests\Geos.
���� � ������� ����� �������� ���������, ��������� � ����� ������� �������� � ������. �� ������� � ���������� \Client\Api\Requests\Geos::$type.
����� \Client\Api\Requests\Geos::getData ���������� ������ � ����������� �������.

##### ��������
��������� ��� ������ ������� � ������ \Client\Api\Manager::$methods

```
Geos::TYPE => [
    'method' => 'geo',
    'type' => 'continuous_get',
],
```
method - ����� ���� �������, ��������������� ����� (� ������ - ��� ����� �����).
type - ��� �������

###### ���� ��������
get - ������� ������ ���� GET
continuous_get - ������������� ������� ���� GET, � ������� � ������� ���������� offset, count ������������� ���� ����� ������ �����������. ������ ������������ � ������ ����������, � ������� ����������.
post - ������ ���� POST

##### ��������� ��� ������ ������

���� ����� ��������� - ���������� ������. �� �������� � \Client\Api\Entities\Geo
���� � ������ ������ ������, ���������� ��������� ������. �� �������� � \Client\Api\Entities\Geos

� ����� setData ������ ��� ��������� ������ ������ �� ������ ������.

##### ������
� ������ Service ������� �����, � ������� ����������� ���������� ��������� �������. ���� ����� ���������� ������ ������.
�� �������� � \Client\Api\Service::getGeos.

#### ���������� � ������� ��������

� AppServiceProvider, � ������ register ������ http ������.
```
    $this->app->bind(HttpClient::class, function() {
        return new HttpClient();
    });
```
��� �� - ������ ��� ������. ��������� ��� ������ (����, �����, ���� �����) ����� �� �������.
```
    $this->app->bind(Service::class, function () {
        $httpClient = $this->app->make(HttpClient::class);
        $httpClient->setUserAgent(config('api.user_agent'));

        $provider = new Provider($httpClient, config('api.token'), config('api.host'));
        $manager = new Manager($provider);

        return new Service($manager);
    });
```