<?php

namespace Client\Api\Requests;

abstract class Request
{
    const TYPE = '';

    public function getType()
    {
        return static::TYPE;
    }

    protected function validate()
    {
        return true;
    }

    public function getData()
    {
        return '';
    }
}
