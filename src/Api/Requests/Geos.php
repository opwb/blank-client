<?php declare(strict_types=1);

namespace Client\Api\Requests;

class Geos extends Request
{
    const TYPE = 'geos';

    /** @var string */
    private $type;

    /**
     * @param string $type
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }

    public function getData()
    {
        return parent::getData().'&type='.$this->type;
    }
}
