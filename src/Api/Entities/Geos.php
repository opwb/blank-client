<?php

namespace Client\Api\Entities;

use Illuminate\Support\Collection;

class Geos extends Collection
{
    /**
     * @param array $geos
     *
     * @return self
     */
    public function setData(array $geos)
    {
        $geos = array_get($geos, 'data.entries');
        if ($geos === null) {
            return $this;
        }

        foreach ($geos as $data) {
            $review = new Geo();
            $this->push($review->setData($data));
        }

        return $this;
    }
}
