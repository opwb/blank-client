<?php

namespace Client\Api\Entities;

class Geo extends Entity
{
    /** @var int */
    private $id;

    /** @var int */
    private $parentId;

    /** @var int */
    private $left;

    /** @var int */
    private $right;

    /** @var string */
    private $type;

    /** @var string */
    private $name;

    /** @var float */
    private $latitude;

    /** @var float */
    private $longitude;

    /**
     * @param  array $data
     *
     * @return self
     */
    public function setData(array $data)
    {
        $this->id = (int)array_get($data, 'id');
        $this->parentId = array_get($data, 'parent_id');
        $this->left = (int)array_get($data, 'left');
        $this->right = (int)array_get($data, 'right');
        $this->type = array_get($data, 'type');
        $this->name = array_get($data, 'name');
        $this->latitude = array_get($data, 'latitude');
        $this->longitude = array_get($data, 'longitude');

        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * @return int
     */
    public function getLeft()
    {
        return $this->left;
    }

    /**
     * @return int
     */
    public function getRight()
    {
        return $this->right;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }
}
