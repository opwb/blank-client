<?php

namespace Client\Api\Engines;

use Client\Api\Responses\Handlers\Composite;

/**
 * @property Composite $responseHandler
 */
class ContinuousGet extends Get
{
    private $iterationLimit = 50;

    private $offset = 0;

    private $delay = 500000; // ms

    private $response = [];

    public function request()
    {
        $this->httpClient->setUrl($this->getUrl());
    }

    /**
     * @return array
     */
    public function response()
    {
        do {
            $this->httpClient->setUrl($this->getUrl());
            $currentResponse = parent::response();
            $continue = !$this->responseHandler->isEmpty($currentResponse);
            if ($continue) {
                yield $currentResponse;
//                $this->response = $this->responseHandler->mergeResponses($this->response, $currentResponse);
                $this->increaseOffset();
                $this->delay();
            }

            $continue = $continue && !$this->responseHandler->limitReached($this->response, $this->limit);
        } while ($continue);

        $this->response = $this->responseHandler->limitResponse($this->response, $this->limit);

        return $this->response;
    }

    protected function increaseOffset()
    {
        // todo использовать параметры из ответа
        $this->offset += $this->iterationLimit;
    }

    protected function delay()
    {
        usleep($this->delay);
    }

    protected function getUrl()
    {
        return parent::getUrl() . '&offset=' . $this->offset . '&count=' . $this->iterationLimit;
    }
}
