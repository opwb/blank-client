<?php

namespace Client\Api;

use Client\Api\Entities\Geo;
use Client\Api\Entities\Geos;
use Client\Api\Requests\Request;
use Client\Api\Requests\Geos as GeosRequest;

class Service
{
    /**
     * @var Manager
     */
    private $manager;

    /**
     * @param Manager $manager
     */
    public function __construct($manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param string $type
     *
     * @return Geos|Geo[]
     */
    public function getGeos(string $type)
    {
        $request = new GeosRequest;
        $request->setType($type);

        foreach ($this->process($request) as $data) {
            $geos = (new Geos())->setData($data);
            foreach ($geos as $geo) {
                yield $geo;
            }
        }
    }

    /**
     * @param Request $request
     * @param int|null $limit
     *
     * @return array
     */
    private function process(Request $request, $limit = null)
    {
        try {
            foreach ($this->manager->process($request) as $item) {
                yield $item->getData();
            }
        } catch (Exception $exception) {
        }

        return [];
    }
}
